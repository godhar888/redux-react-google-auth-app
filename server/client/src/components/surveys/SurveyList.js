import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchSurveys } from '../../actions';

export class SurveyList extends Component {
  componentDidMount() {
    console.log('props at component mounting....');
    console.log(this.props);
    this.props.fetchSurveys();
  }

  renderSurveys() {
    return (
      this.props.surveys.length &&
      this.props.surveys.map(survey => {
        return (
          <div className="card blue-grey darken-1" key={survey._id}>
            <div className="card-content">
              <span className="card-title">{survey.title}</span>
              <p>{survey.body}</p>
              <p className="right">
                Sent On: {new Date(survey.dateSent).toLocaleDateString()}
              </p>
            </div>
            <div className="card-action">
              <a>Yes: {survey.yes}</a>
              <a>No: {survey.no}</a>
            </div>
          </div>
        );
      })
    );
  }

  render() {
    return <div>{this.renderSurveys()}</div>;
  }
}
//return of mapStateToProps must be an object
function mapStateToProps({ surveys }) {
  return { surveys };
}

//connect function returns a component
export default connect(mapStateToProps, { fetchSurveys })(SurveyList);
