const express = require("express");
const mongoose = require("mongoose");
const cookieSessions = require("cookie-session");
const passport = require("passport");
const bodyParser = require("body-parser");

const keys = require("./config/keys");

require("./models/User");
require("./models/Survey");
require("./services/passport");

mongoose.connect(keys.mongoDbUri, { useMongoClient: true });
const app = express();

app.use(bodyParser.json());

app.use(
  cookieSessions({
    maxAge: 30 * 24 * 60 * 60 * 1000,
    keys: [keys.cookieKey]
  })
);
app.use(passport.initialize());
app.use(passport.session());

require("./routes/authRoutes")(app);
require("./routes/billingRoutes")(app);
require("./routes/surveyRoutes")(app);

//went to below request and set in - Authorize requests using OAuth 2.0
//https://developers.google.com/apis-explorer/?hl=en_US#p/plus/v1/

//console.developers.google.com

if (process.env.NODE_ENV === "production") {
  //serve prod assets
  app.use(express.static("client/build"));
  //express will serve index.html if it doesnt recognise the route
  const path = require("path");
  app.get("*", (req, res) => {
    res.sendFile(path.resolve(__dirname, "client", "build", "index.html"));
  });
}

const PORT = process.env.PORT || 5000;

app.listen(PORT);
