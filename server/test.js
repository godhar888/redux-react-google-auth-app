export const FETCH_SURVEYS = 'fetch_surveys';

//from action passed to reducer
export default function(state = [], action) {

  switch (action.type) {
    case FETCH_SURVEYS:
      return action.payload;
    default:
      return state;
  }
}


export default combineReducers({
  auth: authReducer,
  form: reduxForm,
  surveys: surveysReducer
});

///from here action is called
export const fetchSurveys = () => async dispatch => {
  const res = await axios.get('/api/surveys');
  console.log(res);
  dispatch({ type: FETCH_SURVEYS, payload: res.data.surveys });
};


export class SurveyList extends Component {
  componentDidMount() {
    this.props.fetchSurveys();
  }

  renderSurveys() {
    // return this.props.surveys.map(survey => {
    //   return (
    //     <div className="card darken-1" key={survey._id}>
    //       <div className="card-content">
    //         <span className="card-title">{survey.title}</span>
    //         <p>{survey.body}</p>
    //         <p className="right">
    //           Sent On: {new Date(survey.dateSent).toLocaleDateString()}
    //         </p>
    //       </div>
    //       <div className="card-action">
    //         <a>Yes: {survey.yes}</a>
    //         <a>No: {survey.no}</a>
    //       </div>
    //     </div>
    //   );
    // });
  }

  render() {
    // return <div>{this.renderSurveys()}</div>;
  }
}

function mapStateToProps({ surveys }) {
  return { surveys };
}
export default connect(mapStateToProps, { fetchSurveys })(SurveyList);
